## What is Markdown ?
Markdown is a way of writing rich-text (formatted text) content using plain text formatting syntax. 
Markdown is now one of the world’s most popular markup languages.
Using Markdown is different than using a WYSIWYG editor. In an application like Microsoft Word, you click buttons to format words and phrases, and the changes are visible immediately. Markdown isn’t like that. When you create a Markdown-formatted file, you add Markdown syntax to the text to indicate which words and phrases should look different.